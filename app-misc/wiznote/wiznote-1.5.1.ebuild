# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit cmake-utils

DESCRIPTION=""
HOMEPAGE="http://www.wiz.cn/"
SRC_URI="https://codeload.github.com/WizTeam/WizQTClient/tar.gz/v${PV} -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-qt/qtcore:4
	dev-qt/qtgui:4
	dev-qt/qtxmlpatterns:4
	dev-qt/qtwebkit:4
	dev-cpp/clucene
	dev-db/sqlite:3"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	mv "${WORKDIR}"/WizQTClient-${PV} "${S}"
}
